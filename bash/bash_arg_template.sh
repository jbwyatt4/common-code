#!/usr/bin/env bash

USER=~

function _usage() {
	printf '%s\n' \
	'usage:  -h' \
    '-h      : print help screen' \
    '-       : an option, fill me out'
}

if [ $# -ge 1  ]; then
	if [ "$1" == "-h" ]; then
		_usage;
	elif [ "$1" == "-" ]; then
		echo "fill me out"
	else
		echo "Argument not found $1"
		_usage;
	fi
else
	_usage;
fi
